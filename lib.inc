%define EXIT 60
%define EOF 4
section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax,EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0
        je .return
        inc rax
        jmp .loop
    .return:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rsp
	push 0
	mov rax, rdi
	mov rcx, 10
	.loop: 
	    xor rdx, rdx
	    div rcx 
	    add rdx, '0' 
	    dec rsp 
	    mov byte [rsp], dl
	    cmp rax, 0 
	    je .end 
	    jmp .loop
	.end:
	    mov rdi, rsp
	    call print_string
	    mov rsp, r8
	    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .print
    mov r9, rdi
    mov rdi, '-'
    call print_char
    mov rdi, r9
    neg rdi
    .print:
        call print_uint     
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx				; текущий индекс символа
    xor rax, rax
    
    .loop:
        mov al, byte[rdi + rcx]
        cmp al, byte[rsi + rcx]      		; Проверка на равные символы,
        jne .ret_false				; если символы не равны возвращаем false
        cmp al, 0				; проверяет является ли символ конечным в обеих строках,
        je .ret_true				; если является, то возвращаем true
        inc rcx				; иначе продолжает сравнение
        jmp .loop

    .ret_false:
        mov rax, 0
        ret

    .ret_true:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0 
    mov rsi, rsp 
    mov rdx, 1 
    xor rdi, rdi 
    xor rax, rax 
    syscall 
    pop rax 
    cmp rax, EOF 
    jne .not_eof
    xor rax, rax
    .not_eof:
    	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    .loop_leading:
        push rsi
        push rdi
        call read_char
        pop rdi 
        pop rsi
        cmp al, `\t`
        je .loop_leading
        cmp al, `\n`
        je .loop_leading
        cmp al, ' '
        je .loop_leading
        cmp al, 0
        je .fail
        xor r8, r8 
    .loop_main:
        mov byte [rdi+r8], al 
        inc r8 
        cmp rsi, r8 
        je .fail
        push rsi
        push rdi 
        call read_char
        pop rdi 
        pop rsi
        cmp al, `\t` 
        je .success
        cmp al, `\n`
        je .success
        cmp al, ' '
        je .success
        cmp al, 0 
        je .success
        jmp .loop_main
    .success: 
        mov byte [rdi+r8], 0 
        mov rax, rdi 
        mov rdx, r8 
        ret
    .fail: 
        xor rax, rax 
        xor rdx, rdx 
        ret
    
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rax, 0
    xor rcx, rcx	; индекс символа
    xor r8, r8		; будет находится анализируемый символ
    
    .loop:
        mov r8b, byte[rdi + rcx]
        cmp r8, 0x30			; проверяет является ли символ
        jb .stop			; символом цифры
        cmp r8, 0x39			; цифры в таблице ASCII с номерами 0x30 - 0x39
        ja .stop
        
        and r8b, 0xf			; превращает символ в цифру
        mov r9, 10
        mul r9				; добавляет разряд 
        add rax, r8			; rax = rax * 10 + r8
        
        inc rcx			; переходит к следующему символу
    	jmp .loop
    	
    .stop:
    	mov rdx, rcx
    	
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor r8, r8			; проверяет начинается ли строка с минуса
    mov r8b, byte[rdi]
    cmp r8, '-'
    je .parse_neg		; если число положительное использует функцию написанную ранее
    call parse_uint
    ret 
    
    .parse_neg:		; иначе сдвигает указатель
    	add rdi, 1
    	call parse_uint
    	neg rax		; после инвертирует значение
    	add rdx, 1
    	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi 
    push rsi
    push rdx
    call string_length 
    pop rdx 
    pop rsi
    pop rdi
    cmp rax, rdx
    ja .err
    xor rdx, rdx 
    xor rcx, rcx
    .loop:
    mov cl, byte [rdi+rdx]
    mov byte [rsi+rdx], cl
    cmp cl, 0 
    je .success
    inc rdx
    jmp .loop
    .err: 
    	xor rax, rax 
    .success: 
    	ret
